# Unity3D JS Snippets
#### for [Sublime Text 2](http://www.sublimetext.com/2)

## About
This sublime Text 2 package is based on Unity3D Snippets and Completes by [Jacob Pennock](http://jacobpennock.com). It features even more snippets for Unity Javascript.

## Usage
Start typing! functions will autocomplete while you write code

## Install

Just copy the folder inside /Packages in your Sublime Text 2 Installation folder.


## Author
[Jano Amigo](http://janoamigo.com/)
